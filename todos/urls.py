from django.urls import path
from todos.views import TodoItemCreateView
from todos.views import TodoItemUpdateView
from todos.views import TodoListCreateView
from todos.views import TodoListDeleteView
from todos.views import TodoListDetailView
from todos.views import TodoListUpdateView
from todos.views import TodoListListView

urlpatterns = [
    path("", TodoListListView.as_view(), name="todos_list"),
    path("<int:pk>/", TodoListDetailView.as_view(), name="todos_detail"),
    path("create/", TodoListCreateView.as_view(), name="todos_create"),
    path("<int:pk>/edit/", TodoListUpdateView.as_view(), name="todos_edit"),
    path("<int:pk>/delete/", TodoListDeleteView.as_view(), name="todos_delete"),
    path("items/create/", TodoItemCreateView.as_view(), name="items_create"),
    path("items/<int:pk>/edit/", TodoItemUpdateView.as_view(), name="items_edit"),
]
